FROM jrei/systemd-ubuntu

COPY hello-world.sh /root/hello-world.sh
COPY hello-world.service /etc/systemd/system/hello-world.service
COPY hello-world.timer /etc/systemd/system/hello-world.timer

RUN systemctl enable hello-world.timer

CMD ["/lib/systemd/systemd", "--default-standard-output=journal+console", "--default-standard-error=journal+console"]
